const path = require('path');

module.exports = {
    /**
     * Have webpack start parsing the source tree from this (entry) file.
     */
    entry: './src/index.ts',

    /**
     * For every import i.e. "from 'three'" see if there is a file three.ts and grab it.
     * If no .ts file exists see if there is a file three.js and grab it.
     */
    resolve: {
        extensions: [ '.ts', '.js' ],
    },

    /**
     * If the grabbed file ends with .ts, then process it with the TypeScript module (ts-loader).
     */
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: 'ts-loader'
            }
        ]
    },

    /**
     * Combine all the resources into a single dist/bundle.js
     */
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },

    /**
     * Generate inline source maps for easier debugging
     * Which produces very large unoptimized output bundles, but includes debug "symbols"
     */
    devtool: 'inline-source-map',
};