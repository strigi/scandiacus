import Vector from './vector';

class Canvas {
    constructor(selector: string) {
        const foo = document.querySelector(selector);
        const v = new Vector(1, 2, 3);
        console.log(v.length());
    }
}

const c = new Canvas('#foo');