import Vector from "./vector";

describe('Vector', () => {
    describe('constructor', () => {
        it('sets x y and z coordinates', () => {
            const v = new Vector(1, 2, 3);
            expect(v).toHaveProperty('x', 1);
            expect(v).toHaveProperty('y', 2);
            expect(v).toHaveProperty('z', 3);
        });
    });

    describe('statics', () => {
        it('origin is defined as [0, 0, 0]', () => {
            expect(Vector.ORIGIN).toHaveProperty('x', 0);
            expect(Vector.ORIGIN).toHaveProperty('y', 0);
            expect(Vector.ORIGIN).toHaveProperty('z', 0);
        });

        it('origin is defined as [1, 0, 0]', () => {
            expect(Vector.X_AXIS).toHaveProperty('x', 1);
            expect(Vector.X_AXIS).toHaveProperty('y', 0);
            expect(Vector.X_AXIS).toHaveProperty('z', 0);
        });

        it('origin is defined as [0, 1, 0]', () => {
            expect(Vector.Y_AXIS).toHaveProperty('x', 0);
            expect(Vector.Y_AXIS).toHaveProperty('y', 1);
            expect(Vector.Y_AXIS).toHaveProperty('z', 0);
        });

        it('origin is defined as [0, 0, 1]', () => {
            expect(Vector.Z_AXIS).toHaveProperty('x', 0);
            expect(Vector.Z_AXIS).toHaveProperty('y', 0);
            expect(Vector.Z_AXIS).toHaveProperty('z', 1);
        });
    });

    describe('clone()', () => {
        it('returns a duplicate of this', () => {
            const a = new Vector(1, 2, 3);
            const b = a.clone();
            expect(b).toHaveProperty('x', 1);
            expect(b).toHaveProperty('y', 2);
            expect(b).toHaveProperty('z', 3);
        });

        it('is not the same instance of this', () => {
            const a = new Vector(1, 2, 3);
            const b = a.clone();
            expect(a == b).toBe(false);
        });
    });

    describe('length()', () => {
        it('is calculated correctly', () => {
            expect(new Vector(1, 2, 3).length()).toBe(3.7416573867739413);
        });

        it('unit vector has length one', () => {
            expect(new Vector(0, 0, 1).length()).toBe(1);
        });

        it('origin has length zero', () => {
            expect(new Vector(1, 2, 3).length()).toBe(3.7416573867739413);
        });
    });

    describe('add()', () => {
        it('adds that vector to this vector', () => {
            const a = new Vector(1, 2, 3);
            const b = new Vector(4, 5, 6);
            a.add(b);
            expect(a).toHaveProperty('x', 5);
            expect(a).toHaveProperty('y', 7);
            expect(a).toHaveProperty('z', 9);
        });

        it('returns this vector (same instance)', () => {
            const a = new Vector(1, 2, 3);
            const b = new Vector(4, 5, 6);
            const c = a.add(b);
            expect(a == c).toBe(true);
        });

        it('throws error when null or undefined is passed', () => {
            const a = new Vector(5, 8, 7);
            expect(() => a.add(null)).toThrowError('Unable to add null to (5, 8, 7)');
            expect(() => a.add(undefined)).toThrowError('Unable to add undefined to (5, 8, 7)');
        });
    });

    describe('subtract()', () => {
        it('subtracts that vector from this vector', () => {
            const a = new Vector(1, 2, 3);
            const b = new Vector(-3, 5, 8);
            a.subtract(b);
            expect(a).toHaveProperty('x', 4);
            expect(a).toHaveProperty('y', -3);
            expect(a).toHaveProperty('z', -5);
        });

        it('returns this vector (same instance)', () => {
            const a = new Vector(1, -2, -3);
            const b = new Vector(-3, 7, 2);
            const c = a.subtract(b);
            expect(a == c).toBe(true);
        });

        it('throws error when null or undefined is passed', () => {
            const a = new Vector(5, 8, 7);
            expect(() => a.subtract(null)).toThrowError('Unable to subtract null from (5, 8, 7)');
            expect(() => a.subtract(undefined)).toThrowError('Unable to subtract undefined from (5, 8, 7)');
        });
    });

    describe('multiply()', () => {
        it('subtracts that vector from this vector', () => {
            const a = new Vector(1, 2, -3);
            a.multiply(5);
            expect(a).toHaveProperty('x', 5);
            expect(a).toHaveProperty('y', 10);
            expect(a).toHaveProperty('z', -15);
        });

        it('returns this vector (same instance)', () => {
            const a = new Vector(-15, -72, 3);
            const b = a.multiply(-3);
            expect(a == b).toBe(true);
        });
    });

    describe('normalize()', () => {
        it('scales this vector to be length 1', () => {
            const v = new Vector(15, 7, 83);
            expect(v.normalize().length()).toBe(1);
        });

        it('scales coordinates proportional to length', () => {
            const v = new Vector(15, 7, 83);
            expect(v.normalize()).toEqual({x: 0.17723267135653853, y: 0.08270857996638464, z: 0.9806874481728465});
        });

        it('throws error if zero vector', () => {
            const o = Vector.ORIGIN;
            expect(o.normalize.bind(o)).toThrowError('Unable to normalize zero vector');
        });

        it('return this vector (same instance)', () => {
            const a = new Vector(5, 4, 3);
            const b = a.normalize();
            expect(a == b).toBe(true);
        });
    });

    describe('dot()', () => {
        it('calculates the dot product of two vectors', () => {
            const a = new Vector(1, 3, -5);
            const b = new Vector(4, -2, -1);
            expect(a.dot(b)).toBe(3);
        });

        it('throws error when null or undefined is passed', () => {
            const a = new Vector(5, 8, 7);
            expect(() => a.dot(null)).toThrowError('Unable to calculate dot product of (5, 8, 7) with null');
            expect(() => a.dot(undefined)).toThrowError('Unable to calculate dot product of (5, 8, 7) with undefined');
        });
    });

    describe('cross()', () => {
        it('calculates the cross product of two vectors', () => {
            const a = new Vector(1, 2, 3);
            const b = new Vector(3, 4, 5);
            expect(a.cross(b)).toEqual({x: -2, y: 4, z: -2});
        });

        it('return this vector (same instance)', () => {
            const a = new Vector(5, 8, 7);
            const b = new Vector(-5, 17, 27);
            const c = a.cross(b);
            expect(a == c).toBe(true);
        });

        it('throws error when null or undefined is passed', () => {
            const a = new Vector(5, 8, 7);
            expect(() => a.cross(null)).toThrowError('Unable to calculate cross product of (5, 8, 7) with null');
            expect(() => a.cross(undefined)).toThrowError('Unable to calculate cross product of (5, 8, 7) with undefined');
        });
    });

    describe('toString()', () => {
        it('returns string prepresentation of Vector', () => {
            expect(new Vector(1, 2, 3).toString()).toBe('(1, 2, 3)');
        });
    });
});