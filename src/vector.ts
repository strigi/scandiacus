/**
 * Represents a three component vector [x, y, z].
 * This is commonly used in two possible interpretations:
 * 1.) Representing a point (position) in 3D space.
 * 2.) Representing a direction and a size (magnitude) as an "arrow" from the origin [0, 0, 0] to the point [x, y, z].
 * @see https://en.wikipedia.org/wiki/Euclidean_vector
 */
export default class Vector {
    static readonly ORIGIN = new Vector(0, 0, 0);
    static readonly X_AXIS = new Vector(1, 0, 0);
    static readonly Y_AXIS = new Vector(0, 1, 0);
    static readonly Z_AXIS = new Vector(0, 0, 1);

    constructor(public x: number, public y: number, public z: number) {
    }

    clone(): Vector {
        return new Vector(this.x, this.y, this.z);
    }

    /**
     * Returns the length (aka size, magnitude, norm) of this vector.
     * Length is defined as the distance from the origin to the position of the vector.
     * @see https://en.wikipedia.org/wiki/Euclidean_vector#Length
     */
    length() {
        return Math.sqrt(this.x ** 2 + this.y ** 2 + this.z ** 2);
    }

    /**
     * Adds the specified vector to this one, modifying the this vector.
     * @param that The vector to add to this.
     * @return this vector, useful for fluent api (e.g. chaining).
     */
    add(that: Vector): Vector {
        if(!that) {
            throw new Error(`Unable to add ${that} to ${this}`);
        }

        this.x += that.x;
        this.y += that.y;
        this.z += that.z;
        return this;
    }

    subtract(that: Vector): Vector {
        if(!that) {
            throw new Error(`Unable to subtract ${that} from ${this}`);
        }

        this.x -= that.x;
        this.y -= that.y;
        this.z -= that.z;
        return this;
    }

    multiply(scalar: number): Vector {
        this.x *= scalar;
        this.y *= scalar;
        this.z *= scalar;
        return this;
    }

    /**
     * Scales this vector so that it's length is exactly 1.
     * This is done by dividing it by it's own length.
     */
    normalize(): Vector {
        const l = this.length();
        if(l === 0) {
            throw new Error('Unable to normalize zero vector');
        }

        this.x /= l;
        this.y /= l;
        this.z /= l;
        return this;
    }

    dot(that: Vector): number {
        if(!that) {
            throw new Error(`Unable to calculate dot product of ${this} with ${that}`);
        }

        return this.x * that.x + this.y * that.y + this.z * that.z;
    }

    cross(that: Vector) {
        if(!that) {
            throw new Error(`Unable to calculate cross product of ${this} with ${that}`);
        }

        const x = this.y * that.z - this.z * that.y;
        const y = this.z * that.x - this.x * that.z;
        const z = this.x * that.y - this.y * that.x;
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    toString() {
        return `(${this.x}, ${this.y}, ${this.z})`
    }
}